Guide to contributing to Taupunkt / Dew Point for Android
==========================================================

The App has no ads. The Android (Java) sources are written by me. 

I have also included screenshots of the App running on some
different Android devices.

The App is currently also in the Google Play Store, and has been there since
2011. I try to maintain the version there for as long as I have time to do it.


I have stopped development on this app myself, because I think it is quite
complete. If someone wants to further contribute I suggest following
improvements:

1. Translate the included help/documentation of all settings into more languages.

2. Improve the expert mode

3. Add and correct files, so that it can be included in F-Droid.


.... The gradle compile thing works. 
I have started a file de.hoffmannsgimmickstaupunkt.txt which needs to be improved.
Also The build.gradle needs to be adapted, such that the versioning, key-signing
and the package name matches the requirement of F-Droid. ...



best regards
Markus Hoffmann, October 2015

Thanks to Bernd Kuemmel, IMFUFA, Roskilde University Centre, 
PB 260, DK-4000 Roskilde 
for explaining the Dew Point formula on this page:

http://www.faqs.org/faqs/meteorology/temp-dewpoint/

