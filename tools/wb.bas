' X11-Basic program to test the formulas for dew point and
' saturation pressure and wet bulb temperature.
'
' (c) Markus Hoffmann
'
' The formula for wet bulb temperature used gives only an approximation of
' the wet bulb temperature which is close to the exact value, only in case the wet
' bulb temperature is near the dew point. That means, that the relative humidity
' should be larger than 50%. In this case the accuracy is about <2%. If you can
' tolerate a larger error, the formula can be used down to 25% rel. hum.
'

pressure=1000
t=8
r=50

FOR r=1 TO 100
  IF T>=0
    a=7.5
    b=237.3
  ELSE
    a=7.6
    b=240.7
  ENDIF
  sdd=6.1078*10^((a*T)/(b+T))
  dd=r/100*sdd
  v=log10(dd/6.1078)
  td=b*v/(a-v)
  gamma=0.00066*pressure*0.1
  delta=4098.0*dd*0.1/(td+237.3)/(td+237.3)
  twb=(gamma*T+delta*td)/(gamma+delta)

  ' print "Saettigungsdampfdruck: ";sdd;" hPa"

  PRINT t,r,td,twb
  'print "Check:"

  ' ew=0.611*EXP(17.27*twb/(twb+237.3))
  ew=0.611*10^(7.5*twb/(twb+237.3))
  'print ew
  e=ew-gamma*(t-twb)
  'print e
NEXT r
QUIT
